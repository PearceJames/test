<?php
namespace PS\Channels;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Notification;

class NotifierChannelServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $channels = [
            'notifier' => Notifier::class,
        ];
        foreach($channels as $name => $className) {
            Notification::extend($name, function () use ($className) {
                return new $className;
            });
        }
    }
}