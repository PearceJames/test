<?php

namespace PS\Channels;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;

class NotifierChannel
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toMail($notifiable);
        $body = "<h2>$message->greeting</h2><br/>";
        foreach(array_merge($message->introLines, $message->outroLines) as $line) {
            $body .= $line . "<br/>";
        }
        if($message->actionUrl) {
            $body .= "<a href='$message->actionUrl'>$message->actionText</a>";
        }
        $body = base64_encode($body);

        $request = new Request('POST', 'http://notifier/email', [
            "X-PS-Permissions" => "notifier-all",
            "Encoding" => "base64"
        ], json_encode([
            "Address" => $notifiable->email,
            "Subject" => $message->subject,
            "Message" => [
                "Template" => "raw",
                "Vars" => [
                    "Title" => $message->greeting,
                    "Body" => $body
                ]
            ]
        ]));
        $this->client->send($request);
    }
}